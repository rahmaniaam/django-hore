# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Lab2AddonConfig(AppConfig):
    name = 'lab_2_addon'
