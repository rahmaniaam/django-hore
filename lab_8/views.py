from django.shortcuts import render
from django.http import HttpResponseRedirect

response = {}
def index(request):
	response['author'] = "Rahmania Astrid Mochtar"
	html = 'lab_8/lab_8.html'
	return render(request, html, response)