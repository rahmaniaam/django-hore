from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Rahmania Astrid Mochtar' # TODO Implement this
birth_date = date(1998, 3, 26) #TODO Implement this, format (Year, Month, Date)

# Create your views here.
def index(request):
	mhs_age = calculate_age(birth_date.year)
	response = { 'name': mhs_name, 'age': mhs_age }
	return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
	today = date.today()
	year_today = today.year
	return year_today - birth_year
