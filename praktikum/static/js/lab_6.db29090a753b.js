function insertChat(e) {
	var id = e.target.id;
	var notEmpty = $(".usertext").val() != "",
		isEnterKeypress = e.type == "keypress" && e.keyCode == 13,
		isSendClick = e.type == "click" && id == "send";

	if(notEmpty && (isEnterKeypress || isSendClick)) {
		var text = "<b>me: </b>"+ $(".usertext").val();
		var reply = ["<b>your soulmate: </b> Butuh bantuan?",
					"<b>your soulmate: </b> Masa sih",
					"<b>your soulmate: </b> Aku cantik",
					"<b>your soulmate: </b> Kamu ngestalk aku yak",
					"<b>your soulmate: </b> Mantap",
					"<b>your soulmate: </b> Setuju banget",
					"<b>your soulmate: </b> Waaaaaah ga lucuuuuu",
					"<b>your soulmate: </b> Yoeeeh"
					]
		var min = 0;
		var max = 7;
		var random = Math.floor(Math.random() * (max - min + 1)) + min;
		$('<p>' + text + '</p>').addClass("msg-send").appendTo(".msg-insert");
		$('.usertext').val('');
		$('<p>' + reply[random] + '</p>').addClass("msg-receive").appendTo(".msg-insert")
		$('.chat-body').scrollTop($(".chat-body").height());
	}
}
$(".chat-text").keypress(insertChat);

var degree = 0;
var resize = function() {
	$('.chat-body').slideToggle('fast');

	degree += 180;
	$('#arrow-img').css({
		'-webkit-transform': 'rotate(' + degree + 'deg)',
		'-moz-transform': 'rotate(' + degree + 'deg)',
		'-o-transform': 'rotate(' + degree + 'deg)',
		'-ms-transform': 'rotate(' + degree + 'deg)',
		'transform': 'rotate(' + degree + 'deg)'
	});
}

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';

  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
  $('#print').val(print.value).html();
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END


//Theme
var theme = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

$(document).ready(function() {
    $('.my-select').select2();
});

